function checkAge(age) {

	if(age === '') {
		return "Error: Age is Empty!"
	}

	if(age === undefined){
		return "Error: Age should be defined!"
	}

	if(age === null){
		return "Error: Age should not be null"
	}

	if(typeof(age) !== 'number'){
		return "Error: Age should be a number value"
		
	}

	if(age === 0){
		return "Error: Age should not be equal to 0"
	}
	return age;

}

function checkFullname(fullname) {

	if(fullname == '') {
	    console.log(fullname.length)
		return "Error: Fullname is Empty!"
	}

	if(fullname === undefined){
		return "Error: Fullname should be defined!"
	}

	if(fullname == null){
		return "Error: Fullname should not be null"
	}

	if(typeof(fullname) !== 'string'){
		return "Error: Fullname should be a string value"
	}

	if(fullname.length == 0){
		
		return "Error: Fullname length should not be  to 0"
	}


	return fullname;

}


module.exports = {
	checkAge: checkAge,
	checkFullname: checkFullname
}