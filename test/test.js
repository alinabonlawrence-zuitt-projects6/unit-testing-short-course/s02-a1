const {checkAge, checkFullname} = require('../src/util.js');
const {assert} = require('chai');

describe('test_checkAge', () => {
	it('age_not_empty', ()=> {
		const age = "";
		assert.isNotEmpty(checkAge(age));
	});

	it('age_not_defined', () => {
		const age = undefined;
		assert.isDefined(checkAge(age));
	});

	it('age_not_null', () => {
		const age = null;
		assert.isNotNull(checkAge(age));
	});

	it('age_type_integer', () => {
		const age = 1;
		assert.isNumber(checkAge(age));
	});

	it('age_not_0', () => {
		const age = 0;
		assert.notEqual(checkAge(age));
	});
})

describe('test_checkFullname', () => {
	it('fullname_not_empty', ()=> {
		const fullname = "";
		assert.isNotEmpty(checkFullname(fullname)); 
	});

	it('fullname_not_defined', () => {
		const fullname = undefined;
		assert.isDefined(checkFullname(fullname));
	});

	it('fullname_not_null', () => {
		const fullname = null;
		assert.isNotNull(checkFullname(fullname));
	});

	it('fullname_type_string', () => {
		const fullname = 0;
		assert.isString(checkFullname(fullname));
	});
	it('fullname_length_not_0', () => {
		const fullname = "";
		assert.operator(checkFullname(fullname).length, '>' , 0);
	});

})
